# GIT
cd .
git clone https://bitbucket.org/ev45ive/altkom-javascript-marzec.git altkom-javascript-marzec
cd altkom-javascript-marzec

code altkom-javascript-marzec

## GIT update
git stash -u
git pull 

#  Instalacje
node -v
v16.13.1

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1

code -v
Visual Studio Code -> Help -> About
1.65.2

chrome://version/
Google Chrome	99.0.4844.74


## Patterns anipatterns

https://wiki.c2.com/?DesignPatternsBook
https://wiki.c2.com/?AntiPatternsCatalog

https://refactoring.guru/pl/design-patterns/abstract-factory
https://pl.wikipedia.org/wiki/Fabryka_abstrakcyjna
https://sourcemaking.com/design_patterns/abstract_factory   


## Angular

ng g module search --route search --routing -m app

ng g c search/containers/search-view
ng g c search/components/search-form
ng g c search/components/search-results
ng g c search/components/album-card
ng g c search/components/artist-card



### Command - Query Separation Pattern

- Commands
- Query

https://ngrx.io/guide/effects
https://redux.js.org/introduction/getting-started
https://vuex.vuejs.org/guide/actions.html