loading.hidden = true

const tax = 23;

const template = {
    name: '',
    price: 0,
    discount: 0,
    isPromoted: false,
    description: '',
    dateCreated: (new Date()), //.toJSON(),
    reviews: {
        count: 0,
        average: 0
    }
}

const products = [{
    ...template,
    reviews: { ...template.reviews },
    name: 'Placki "Adrian`s" ',
    price: 22.1,
    description: `ala ma kota`,
    dateCreated: (new Date(2020, 2, 20)).toJSON(),
}, {
    ...template,
    reviews: { ...template.reviews },
    name: 'Nalesniki "de`s" ',

    discount: 0.15,
    isPromoted: true,
    price: 20,
    description: 'Super naleśniki',
}, {
    ...template,
    reviews: { ...template.reviews },
    name: 'Placki "Pyszne"',
    discount: 0.25,
    isPromoted: true,
    price: 10.99,
    desc: 'Super pyszne  Musisz spróbowac',
}]

let onlyPromoted = undefined; /* true - only promoted, false - only not, undefined - all */
let per_page = 2;
let page = 1;
let dir = -1 /* 1 - ASC, -1 - DESC */

const compose = (...fns) => x => fns.reduce((acc, fn) => fn(acc), x)
const pipe = (...fns) => x => fns.reduceRight((acc, fn) => fn(acc), x);


const direction = dir => products => dir > 0 ? products.reverse() : products

const sortBy = key => (p1, p2) => p1[key].localeCompare(p2[key], 'pl-PL', { /*  */ });

const filterBroductByPromoted = isPromoted => product => isPromoted !== undefined?  product.isPromoted === isPromoted : true;

const renderToConsole = (count = 1, product) => {
    count++;

    const price = (product.price) * (1 + tax / 100);
    const pricePromo = (price * (1 - product.discount));

    console.log(`${count}. ${product.name} - ${price.toFixed(2)} PLN - ${product.isPromoted ? pricePromo.toFixed(2) + 'PLN' : ''} ${product.description} ${(product.isPromoted) ? 'PROMOTED' : ''}`);
    return count;
};

const renderProducts = (products) => {
    return direction(dir)(products)
        .sort(sortBy('name'))
        .filter(filterBroductByPromoted(onlyPromoted))
        .slice((page - 1) * per_page, page * per_page)
        .reduce(renderToConsole,0)
}

renderProducts(products)


