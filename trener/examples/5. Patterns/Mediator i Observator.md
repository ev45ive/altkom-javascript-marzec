
```js

class Target extends EventTarget{}

target = new Target()
target.addEventListener('test',console.log)
target.dispatchEvent(new Event('test',{}))

// Event  
// isTrusted: false
// bubbles: false
// cancelBubble: false
// cancelable: false
// composed: false
// currentTarget: Target {}
// defaultPrevented: false
// eventPhase: 0
// path: []
// returnValue: true
// srcElement: Target {}
// target: Target {}
// timeStamp: 105900.90000000596
// type: "test"
// [[Prototype]]: Event
```
## Event Bubbling
https://domevents.dev/
