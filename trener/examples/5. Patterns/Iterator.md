
```js

makeIter = {
    [Symbol.iterator](){
        var i  =0
        var max =10
        
        return {
            next(){
                return { value: i++, done: i > max}
            }
        }
    }
}
// {Symbol(Symbol.iterator): ƒ}

for(let item of makeIter){
    console.log(item)
}

```

## Async iterator

```js
makeIter = {
    [Symbol.asyncIterator](){
        var i  =0
        var max =10
        
        return {
            next(){
                return Promise.resolve({ value: i++, done: i > max})
            }
        }
    }
}

for await (let item of makeIter){
    console.log(item)
}
```

## Generator + Iterator
```js

var fibonacci = {
  [Symbol.iterator]: function* () {
    var pre = 0, cur = 1;
    for (;;) {
      var temp = pre;
      pre = cur;
      cur += temp;
      yield cur;
    }
  },
};

for (var n of fibonacci) {
  // truncate the sequence at 10
  if (n > 10) break;
  console.log(n);
}

```

## ASync generator => async iterator

```js
var fibonacci = {
  [Symbol.asyncIterator]: async function* () {
    var pre = 0, cur = 1;
    for (;;) {
      var temp = pre;
      pre = cur;
      cur += temp;
      yield await Promise.resolve(cur);
    }
  },
};

for await (let n of fibonacci) {
  // truncate the sequence at 10
  if (n > 10) break;
  console.log(n);
}
```