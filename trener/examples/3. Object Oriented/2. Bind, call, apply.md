```js

function Person(name){    
    // this = {}
    this.name  = name
    this.sayHello = function(){
        return `Hi, I am ${this.name}`
    }
    return this;
}

// alice = Person.bind({}) ('Alice')
// alice = Person.call({}, 'Alice')
alice = Person.apply({}, ['Alice'])


```