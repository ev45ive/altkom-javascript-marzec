import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchViewComponent } from './containers/search-view/search-view.component';
import { SearchComponent } from './search.component';

const routes: Routes = [{
  path: '', component: SearchComponent,
  children: [
    {
      path: '', 
      component: SearchViewComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
