import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { SearchViewComponent } from './containers/search-view/search-view.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { ArtistCardComponent } from './components/artist-card/artist-card.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http'
@NgModule({
  declarations: [
    SearchComponent,
    SearchViewComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    ArtistCardComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class SearchModule { }
