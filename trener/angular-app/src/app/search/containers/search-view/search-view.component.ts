import { SearchService } from './../../services/search/search.service';
import { SearchCommand } from './../../actions/SearchCommand';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-view',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.css']
})
export class SearchViewComponent implements OnInit {

  results = this.service.results

  constructor(
    private service: SearchService
  ) { }

  ngOnInit(): void {
  }

  search(cmd: SearchCommand) {
    this.service.search(cmd)
  }

}
