import { Component, Input, OnInit } from '@angular/core';
import { Result } from '../../services/search/Result';

@Component({
  selector: 'app-artist-card',
  templateUrl: './artist-card.component.html',
  styleUrls: ['./artist-card.component.css']
})
export class ArtistCardComponent implements OnInit {
  
  @Input() result!: Result

  constructor() { }

  ngOnInit(): void {
  }

}
