import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { SearchCommand } from '../../actions/SearchCommand';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  searchForm = this.fb.group({
    query: this.fb.control('', []),
    type: this.fb.control('album', []),
  })

  @Output() search = new EventEmitter<SearchCommand | SearchCommand>();

  submit() {
    if (this.searchForm.invalid) return;
    this.search.next(new SearchCommand(this.searchForm.value))
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

}
