import { Component, Input, OnInit, QueryList, TemplateRef, ViewChildren } from '@angular/core';
import { Result } from '../../services/search/Result';

interface ResultTemplateCtx {
  $implicit: Result;
}

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  @Input() results: Result[] = []
  context?: ResultTemplateCtx

  outlet: TemplateRef<ResultTemplateCtx> | null = null

  @ViewChildren(TemplateRef)
  templates?: QueryList<ResultTemplateCtx>

  constructor() { }

  ngAfterViewInit() {
    this.templates
    // debugger
  }

  ngOnInit(): void {
  }

}
