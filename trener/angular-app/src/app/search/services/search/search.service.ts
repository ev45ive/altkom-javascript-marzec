import { AuthService } from './../auth/auth.service';
import { ArtistSearchCommand,  AlbumSearchCommand } from './../../actions/SearchCommand';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Result } from './Result';

interface SearchResponse<Result> {
  'albums': { items: Result[] }
  'artists': { items: Result[] }
}

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  private _results = new BehaviorSubject<Result[]>([])
  results = this._results.asObservable()

  handlers = {
    'AlbumSearchCommand': (cmd: AlbumSearchCommand) => {
      const observable = this.http.get<SearchResponse<Result>>('https://api.spotify.com/v1/search', {
        params: cmd.params,
        headers: { Authorization: 'Bearer ' + this.auth.getToken() }
      })

      observable.subscribe({
        next: resp => this._results.next(resp.albums.items)
      })
    }
  }

  search(cmd: ArtistSearchCommand | AlbumSearchCommand) {
    cmd.execute()
    // this.handlers[cmd.type](cmd)

  }
}
