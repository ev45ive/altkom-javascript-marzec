
interface BaseResult {
    id: string, name: string
}

interface Album extends BaseResult { type: 'album', images: { url: string }[] }
interface Artist extends BaseResult { type: 'artist', images: { url: string }[] }


export type Result = Album | Artist
