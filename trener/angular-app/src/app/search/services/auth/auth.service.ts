import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private oauth: OAuthService) { }

  async init() {
    await this.oauth.tryLogin()
    if (!this.oauth.getAccessToken()) {
      this.login()
    }
  }

  login() {
    this.oauth.initLoginFlow()
  }

  getToken() {
    return this.oauth.getAccessToken()
  }

}
