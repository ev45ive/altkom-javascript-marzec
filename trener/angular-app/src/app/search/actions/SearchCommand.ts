
export class AlbumSearchCommand {
  readonly type = 'AlbumSearchCommand';
  constructor(readonly params: { query: string; type: 'album' }) { }
  execute() {
    console.log('Execute', this);
  }
}
export class ArtistSearchCommand {
  readonly type = 'ArtistSearchCommand';
  constructor(readonly params: { query: string; type: 'artist'; }) { }
  execute() {
    console.log('Execute', this);
  }
}
