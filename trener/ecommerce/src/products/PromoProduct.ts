import { ProductDTO } from "./productsData";
import { Product } from "./product";


export class ProductSet extends Product{}
export class DigitalProduct extends Product{}

export class PromoProduct extends Product {
    discount = 0;
    promotion = false

    constructor(
        name = '',
        price = 0,
    ) {
        super(name, price);
    }

    static createProduct(dto: ProductDTO) {
        const prod = new PromoProduct(dto.name, dto.price);
        prod.id == dto.id;
        prod.description == dto.description;
        prod.promotion = dto.isPromoted
        prod.discount = dto.discount
        return prod;
    }
}

