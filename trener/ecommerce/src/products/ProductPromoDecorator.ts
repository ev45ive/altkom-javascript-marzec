import { Product } from "./product";


// export class DigitalProduct extends Product{}

export class ProductPromoDecorator implements Product {
    get id() { return this.product.id; }
    public name = ''
    public _price = 0

    get description() { return this.product.description; }
    get price() {
        return this.product.price * (1 - this.discount);
    }

    constructor(
        protected product: Product,
        public discount = 0,
        public promotion = false
    ) {
    }
}
