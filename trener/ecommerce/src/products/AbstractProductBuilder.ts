import { ProductDTO } from "./productsData";


export abstract class AbstractProductBuilder {
    getProduct(dto: ProductDTO) {

        this.getProductData(dto);
        this.getProductPricing(dto);
        this.getProductReviews(dto);
    }
    getProductReviews(dto: ProductDTO) {
        throw new Error("Method not implemented.");
    }
    getProductPricing(dto: ProductDTO) {
        throw new Error("Method not implemented.");
    }
    getProductData(dto: ProductDTO) {
        throw new Error("Method not implemented.");
    }
}
class ProductData { }
class ProductPricing { }
class ProductReviews { }
