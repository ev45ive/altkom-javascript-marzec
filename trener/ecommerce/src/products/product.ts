/* S.O.L.I.D */

import { ProductDTO } from "./productsData";

export class Product {
    id = ''
    description = ''
    get price() { return this._price }

    constructor(
        public name = '',
        readonly _price: number
    ) { }

    static createProduct(dto: ProductDTO) {
        const prod = new Product(dto.name, dto.price)
        prod.id == dto.id
        prod.description == dto.description
        prod._price == dto.price
        return prod
    }
}

