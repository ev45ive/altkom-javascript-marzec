import { Product } from "./product";



export class ProductComposite extends Product {
    override get price() {
        return this.compositeParts.reduce((sum, product) => {
            return sum + product._price
        }, 0)
    }

    constructor(
        public name: string,
        public description: string,
        public compositeParts: Product[] = [],
    ) {
        super(name, 0)
    }
}
