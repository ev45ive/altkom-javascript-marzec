import { Product } from "./product";
import { PromoProduct } from "./PromoProduct";
import { ProductDTO } from "./productsData";
import { AbstractProductFactory } from "./AbstractProductFactory";


export class Products {

    items: Product[] = []

    // Abstract factory
    productFactory: AbstractProductFactory = Product

    createProducts(data: ProductDTO[]) {
        this.items = data.map(dto => this.productFactory.createProduct(dto))
        // this.productFactory.createReview()
    }
}

// class PromoProducts extends Products {
//     productFactory: AbstractProductFactory = PromoProduct
// }

export default Products
