import { ProductDTO } from "./productsData";
import { PromoProduct } from "./PromoProduct";

class SpecialKnowledgeProductFactory {
    constructor(private helpers: any) { }

    static createProduct(dto: ProductDTO) {
        // this.helpers....
        // special changes with special knowledge
        return PromoProduct.createProduct(dto);
        // return {name:'MockProduct'} as Product
    }
}
