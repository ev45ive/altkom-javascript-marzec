import { AbstractProductFactory } from "./AbstractProductFactory";
import { Product } from "./product";
import { ProductDTO } from "./productsData";
import { PromoProduct } from "./PromoProduct";

class BaseProductFactories implements AbstractProductFactory {
    constructor(config: {}, services: {}) { }

    createProduct(dto: ProductDTO): Product {
        throw 'Not implemented'
    }
    createDiscount(dto: ProductDTO) { }
    createReview(dto: ProductDTO) { }

    // create(dto: ProductDTO): Product {   }
    // create(dto: ProductDTO) { }
    // create(dto: ProductDTO) { }
}

class SpeciaBaseProductFactories extends BaseProductFactories {
    createProduct(dto: ProductDTO): PromoProduct {
        throw 'Not implemented'
    }
    createDiscount(dto: ProductDTO) { }
}
