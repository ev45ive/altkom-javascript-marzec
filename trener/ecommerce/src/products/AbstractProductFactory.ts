import { Product } from "./product";
import { ProductDTO } from "./productsData";

export interface AbstractProductFactory {
    createProduct(dto: ProductDTO): Product;
}
