export interface ProductDTO {
    id: string;
    name: string;
    price: number;
    discount: number;
    isPromoted: boolean;
    description: string;
    dateCreated: string;
    reviews: {
        count: number;
        average: number;
    };
}


export const productsData: ProductDTO[] = [
    {
        "id": '123',
        "name": "Placki \"Adrian`s\" ",
        "price": 22.1,
        "discount": 0,
        "isPromoted": false,
        "description": "ala ma kota",
        "dateCreated": "2020-03-19T23:00:00.000Z",
        "reviews": {
            "count": 0,
            "average": 0
        }
    },
    {

        "id": '234',
        "name": "Nalesniki \"de`s\" ",
        "price": 20,
        "discount": 0.15,
        "isPromoted": true,
        "description": "Super naleśniki",
        "dateCreated": "2022-03-23T08:39:44.241Z",
        "reviews": {
            "count": 0,
            "average": 0
        }
    },
    {

        "id": '345',
        "name": "Placki \"Pyszne\"",
        "price": 10.99,
        "discount": 0.25,
        "isPromoted": true,
        "description": "",
        "dateCreated": "2022-03-23T08:39:44.241Z",
        "reviews": {
            "count": 0,
            "average": 0
        },
    }
];
