

export class SessionSingleton {
    private constructor() { }

    private static _instance: SessionSingleton;

    static getInstance() {
        if (!SessionSingleton._instance) {
            SessionSingleton._instance = new SessionSingleton();
        }
        return SessionSingleton._instance;
    }
}


// ;((window)=>{

//     var instance;

//     window.getInstance = () => {
//         if (!_instance) {
//             _instance = new SessionSingleton();
//         }
//         return _instance;
//     }

// })(window)